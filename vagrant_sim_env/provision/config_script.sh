#!/usr/bin/env bash

echo "1 Installing python........................................................................1"
sudo apt-get -y install python

echo "2 Updating Machine.........................................................................2"
sudo apt-get -y update

echo "3 Installing pip...........................................................................3"
sudo apt-get -y install python-pip

echo "4 Installing dev libraries.................................................................4"
sudo apt-get -y install libffi-dev python-dev libssl-dev

echo "5 Upgrading pip ...........................................................................5"
sudo pip install --upgrade pip

echo "6 Seting up firewall.......................................................................6"
sudo ufw -f enable
sudo ufw allow http
sudo ufw allow ssh/tcp
sudo ufw allow 5432
