#!/usr/bin/env bash


echo "1 Installing python........................................................................1"
sudo apt-get -y install python

echo "2 Updating Machine.........................................................................2"
sudo apt-get -y update

echo "3 Installing pip...........................................................................3"
sudo apt-get -y install python-pip

echo "4 Installing dev libraries.................................................................4"
sudo apt-get -y install libffi-dev python-dev libssl-dev

echo "5 Upgrading pip ...........................................................................5"
sudo pip install --upgrade pip

echo "6 Installing ansible.......................................................................6"
sudo pip install ansible

echo "7 Adding nodes to hosts file...............................................................7"
sudo echo -e '192.168.81.101 dev\n192.168.81.102 db\n192.168.81.103 lb\n192.168.81.104 wbs1\n'\
'192.168.81.105 wbs2' >> /etc/hosts

echo "8 Installing sshpass.......................................................................8"
sudo apt-get -y install sshpass

echo "9 Configuring ssh..........................................................................9"
sudo echo "StrictHostKeyChecking=no" >> /etc/ssh/ssh_config

echo "10 Mounting /vagrant......................................................................10"
sudo mount -tvboxsf vagrant /vagrant

echo "11 Copying ssh-keygen script and vagrant file mount script to /home/ubuntu/...............11"
cp /vagrant/misa/ssh_key_script.sh /home/ubuntu/
cp /vagrant//misa/vag_fol_mount_script.sh /home/ubuntu/

echo "12 Seting up firewall.....................................................................12"
sudo ufw -f enable
sudo ufw allow http
sudo ufw allow ssh/tcp

echo "13 Rebooting Machine......................................................................13"
sudo reboot
